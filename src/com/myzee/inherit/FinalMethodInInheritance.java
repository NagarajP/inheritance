/*
 * Using final with Inheritance
 */
package com.myzee.inherit;

public class FinalMethodInInheritance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BaseClass b = new DerivedClass();
		b.RegularMethod("regularmethod");
	}

}

class BaseClass {
	public void RegularMethod(String text) {
		System.out.println("welcome to baseclass " + text);
	}
	
	public final void FinalMethodImpl() {
		System.out.println("This is base class final method, cannot be overridden");
	}
}

class DerivedClass extends BaseClass{
	public void RegularMethod(String text) {
		System.out.println("welcome to derived " + text);
	}
	
	public void FinalMethodImpl() {	// Compilation error: remove the final modifier from base class method.
		System.out.println("cannot override final method");
	}
	
}
