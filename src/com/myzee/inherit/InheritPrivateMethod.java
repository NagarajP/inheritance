package com.myzee.inherit;

public class InheritPrivateMethod {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Derived d = new Derived();
		d.foo();
//		 ((Base)d).foo();

		Base b = new Derived();
		b.foo(); // visibility is private in base
	}

}

class Base {
	int i = 90;

	private void foo() {
		System.out.println("base method");
	}

	public void call() {
		System.out.println("call method");
	}
}

class Derived extends Base {
	public void foo() {
		super.call();
		System.out.println(super.i);
		System.out.println("derived method");
	}
}
