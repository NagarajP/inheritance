
/*
 * As per the Inheritance law, you cannot inherit the variables from parent to child.
 * So whenever you try to use the variable, it always be accessing based on the 'reference to object' not based on the actual object.
 * So in the below case 
 * Parent par = new Child()
 * par.value; 	-	it always access the parent variable value.
 * 
 * Still you can access the child variable value using parent reference by typecasting.
 * ((Child)par).value
 */

package com.myzee.inherit;

public class ParentRefTypecasting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Parent parObj = new Child();
		System.out.println(((Child)parObj).value);
		System.out.println(((Child)parObj).id);
		System.out.println(parObj.value);
		int i = Integer.decode("16788");
		System.out.println(i);
		i = Integer.parseInt("12345");
		System.out.println(i);
		Double d = Double.parseDouble("1399998.0999999487384783999");	
		System.out.println(d);
	}
}

class Parent {
	String value = "ParentVar";
	int id = 77;
}

class Child extends Parent {
	String value = "ChildVar";
}
