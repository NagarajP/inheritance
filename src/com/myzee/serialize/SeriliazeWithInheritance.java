/*
 * In Inheritance, if the parent class is serializable then child is automatically serializable. No need to implement 
 * serializable to child class explicitly.
 */
package com.myzee.serialize;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SeriliazeWithInheritance {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Child c = new Child(10, 50);
		
		try {
			System.out.println("data members before serialization");
			System.out.println(c.i + " " + c.j);
			FileOutputStream fos = new FileOutputStream("inherit.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(c);
			oos.close();
			fos.close();
			
			FileInputStream fis = new FileInputStream("inherit.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Child c1 = (Child) ois.readObject();
			System.out.println("data members after Deserialization");
			System.out.println(c1.i + " " + c1.j);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

class Parent implements Serializable{
	int i;
	public Parent(int i) {
		this.i = i;
	}
}

class Child extends Parent{
	int j;
	public Child(int i, int j) {
		super(i);
		this.j = j;
	}
}
