/*
 * What happens when a class is serializable but its superclass is not ?
 * 
 * Serialization: At the time of serialization, if any instance variable is inheriting from non-serializable superclass, 
 * then JVM ignores original value of that instance variable and save default value to the file.
 * 
 * De- Serialization: At the time of de-serialization, if any non-serializable superclass is present, 
 * then JVM will execute instance control flow in the superclass. To execute instance control flow in a class, JVM will always invoke default(no-arg) constructor of that class. So every non-serializable superclass must necessarily contain default constructor, otherwise we will get runtime-exception.
 */
package com.myzee.serialize;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SeriliazeWithInheritance1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Derived c = new Derived(10, 50);
		
		try {
			System.out.println("data members before serialization");
			System.out.println(c.i + " " + c.j);
			FileOutputStream fos = new FileOutputStream("inherit.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(c);
			oos.close();
			fos.close();
			
			FileInputStream fis = new FileInputStream("inherit.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Derived c1 = (Derived) ois.readObject();
			System.out.println("data members after Deserialization");
			System.out.println(c1.i + " " + c1.j);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

class Base {
	int i;
	public Base(int i) {
		this.i = i;
	}
	public Base() {
		
	}
}

class Derived extends Base implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int j;
	public Derived(int i, int j) {
		super(i);
		this.j = j;
	}
}
