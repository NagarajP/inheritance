/*
 * If the reference variable is not serialized inside an object, 
 * we will get run time exception saying "java.io.NotSerializableException". 
 */

package com.myzee.serialize;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SeriliazeWithInheritance3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Ser1 s = new Ser1();
		
		try {
			System.out.println("Before serilization");
			System.out.println("i - " + s.s.i);
			System.out.println("j - " + s.s.j);
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("abc.ser"));
			oos.writeObject(s);
			oos.close();
			
			System.out.println("After deserilization");
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("abc.ser"));
			System.out.println("i - " + s.s.i);
			System.out.println("j - " + s.s.j);
			ois.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

class Ser1 implements Serializable {
	Ser2 s = new Ser2(10, 20);
}

//class Ser2 implements Serializable{
class Ser2 {
	int i, j;
	public Ser2(int i, int j) {
		this.i = i;
		this.j = j;
	}
}
